<?php declare (strict_types = 1);

function raiseByPower(float $x, float $n): float
{
    echo "Calling function with $x, $n" . PHP_EOL;
    if ($n == 1) {
        return $x;
    }

    return $x * raiseByPower($x, $n - 1);
}

$result = raiseByPower(2, 4);
echo "Result is $result" . PHP_EOL;
