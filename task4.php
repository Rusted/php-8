<?php declare (strict_types = 1);

function dbd(int $a, int $b): int
{
    if ($a == $b) {
        return $a;
    }

    if ($a < $b) {
        return dbd($a, $b - $a);
    }

    return dbd($a - $b, $b);
}

echo dbd(441, 42);
