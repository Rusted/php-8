<?php declare (strict_types = 1);

const FILE_FOLDER = 'files';
const IMAGE_FILE_EXTENSIONS = ['png', 'jpeg', 'jpg'];

function listFolder(string $directory, string $filename, int $level): void
{
    if ($filename != '..' && $filename != '.') {
        for ($i = 0; $i <= $level; $i++) {
            echo ' - ';
        }
        echo $filename . '<br>';
        listFiles($directory . '/' . $filename, $level + 1);
    }
}

function listFile(string $directory, string $filename, int $level): void
{
    $pathInfo = pathinfo($filename);
    $extension = $pathInfo['extension'];
    for ($i = 0; $i <= $level; $i++) {
        echo ' - ';
    }

    if ($extension == 'php') {
        echo '<a href="readfile.php?file=' . $directory . '/' . $filename . '" >'
            . $filename
            . '</a>'
        ;
    } else if (in_array($extension, IMAGE_FILE_EXTENSIONS)) {
        echo '<a href="viewImage.php?file=' . $directory . '/' . $filename . '" >'
            . $filename
            . '</a>'
        ;
    } else {
        echo $filename;
    }

    echo '<br>';
}

function listFiles(string $directory, int $level = 0): void
{
    $dir = opendir($directory);
    while ($filename = readdir($dir)) {
        if (is_dir($directory . '/' . $filename)) {
            listFolder($directory, $filename, $level);
        }

        if (is_file($directory . '/' . $filename)) {
            listFile($directory, $filename, $level);
        }
    }

    closedir($dir);
}

listFiles(FILE_FOLDER);
