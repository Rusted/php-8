<?php

$navigation = [
    [
        'id' => 1,
        'caption' => 'Apie mus',
        'subMenu' =>
        [
            [
                'id' => 2,
                'caption' => 'Mûsø vizija',
            ],
            [
                'id' => 3,
                'caption' => 'Istorija',
            ],
        ],
    ],
    [
        'id' => 4,
        'caption' => 'Teikiamos paslaugos',
    ],
    [
        'id' => 5,
        'caption' => 'Mûsø darbai',
        'subMenu' =>
        [
            [
                'id' => 6,
                'caption' => 'Tinklapiø kûrimas',
                'subMenu' =>
                [
                    [
                        'id' => 6,
                        'caption' => 'Tinklapiø kûrimas',
                        'subMenu' =>
                        [
                            [
                                'id' => 6,
                                'caption' => 'Tinklapiø kûrimas',
                            ],
                            [
                                'id' => 7,
                                'caption' => 'Programinë áranga',
                                'subMenu' =>
                                [
                                    [
                                        'id' => 6,
                                        'caption' => 'Tinklapiø kûrimas',
                                    ],
                                    [
                                        'id' => 7,
                                        'caption' => 'Programinë áranga',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        'id' => 7,
                        'caption' => 'Programinë áranga',
                    ],
                ],
            ],
            [
                'id' => 7,
                'caption' => 'Programinë áranga',
            ],
        ],
    ],
    [
        'id' => 8,
        'caption' => 'Kontaktai',
    ],
];

function printTree(array $navigation): void
{
    echo "<ul>";
    foreach ($navigation as $navigationItem) {
        echo "<li>" . $navigationItem['caption'] . "</li>";
        if (array_key_exists('subMenu', $navigationItem)) {
            printTree($navigationItem['subMenu']);
        }
    }

    echo "</ul>";
}

printTree($navigation);
