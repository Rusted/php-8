<?php declare (strict_types = 1);

function factorial(int $number): int
{
    echo "Calling factorial with $number" . PHP_EOL;
    if ($number <= 1) {
        return 1;
    }

    return $number * factorial($number - 1);
}

$result = factorial(4);
echo "Result is " . $result . PHP_EOL;
